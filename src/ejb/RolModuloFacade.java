/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.RolModulo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import controller.Login;

@Stateless
public class RolModuloFacade extends AbstractFacade<RolModulo> {
	
	private final String QUERY_ROL_MODULO_LISTA = "select o from RolModulo o "
			+ "where o.estado in ('A') "
			+ "and o.rol.idRol = :idRol ";

    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Inject
    private Login login;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolModuloFacade() {
        super(RolModulo.class);
    }

    @SuppressWarnings("unchecked")
	public List<RolModulo> obtenerRolModuloList(Integer idRol){    	
    	List<RolModulo> listaRolModulo = new ArrayList<>();
    	listaRolModulo = em.createQuery(QUERY_ROL_MODULO_LISTA)
    							.setParameter("idRol", idRol)
				    			.getResultList();
    	return listaRolModulo;
    }
    
    
    public String guardar(RolModulo rolModulo) {
        try {
            rolModulo.setFechaAlta(new Date());
            rolModulo.setUsuAlta(login.getUsuarioConectado().getUsuario());
            em.persist(rolModulo);
            em.flush();
            return "OK";
        } catch (PersistenceException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.toString()));
            return "";
        }
    }

    public String actualizar(RolModulo rolModulo) {
        try {
            rolModulo.setFechaMod(new Date());
            rolModulo.setUsuMod(login.getUsuarioConectado().getUsuario());
            em.merge(rolModulo);
            em.flush();

            return "OK";
        } catch (PersistenceException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.toString()));
            return "";
        }

    }

    public String eliminar(RolModulo rolModulo) {
        try {
            RolModulo rolPant = em.find(RolModulo.class, rolModulo.getIdRolModulo());
            em.remove(rolPant);          
            em.flush();
            return "OK";
        } catch (PersistenceException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.toString()));
            return "";
        }
    }
}
