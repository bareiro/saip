/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Modulo;
import entity.RolModulo;
import entity.UsuarioRol;
import entity.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import controller.Login;

/**
 *
 * @author DESARROLLO
 */
@Stateless
public class ModuloFacade extends AbstractFacade<Modulo> {

    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Inject
    Login login;

    @EJB
    UsuarioRolFacade usuarioRolFacade;
    @EJB
    RolModuloFacade rolModuloFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModuloFacade() {
        super(Modulo.class);
    }

    public List<Modulo> getModuloList(String descripcion, String estado) {
        try {
            String query = "select o from Modulo o where o.estado in ('" + estado + "')  ";

            if (descripcion != null && !descripcion.equals("")) {
                query = query + " and lower(o.descripcion) = '" + descripcion.toLowerCase().trim() + "'";
            }

            return em.createQuery(query).getResultList();

        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException ex) {
            return null;
        }
    }

    /**
     * @author mbareiro obtiene la modulo segun el campo modulo
     * @param descripcion
     * @return
     */
    public Modulo obtenerModulo(String descripcion) {
        try {
            String query = "select o from Modulo o where o.descripcion = :descripcion ";
            return (Modulo) em.createQuery(query)
                    .setParameter("descripcion", descripcion)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException ex) {
            return null;
        }
    }

    public String guardar(Modulo modulo) {
        try {
            modulo.setUsuAlta(login.getUsuarioConectado().getUsuario());
            modulo.setFechaAlta(new Date());
            modulo.setEstado("A");
            em.persist(modulo);
            em.flush();

            return "OK";
        } catch (PersistenceException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.toString()));
            return "";
        }
    }

    public String actualizar(Modulo modulo) {
        try {
            modulo.setFechaMod(new Date());
            modulo.setUsuMod(login.getUsuarioConectado().getUsuario());
            em.merge(modulo);
            em.flush();
            return "OK";
        } catch (PersistenceException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.toString()));
            return "";
        }
    }

    /**
     * @author mbareiro Se obtiene el listado de modulos asociadas a un usuario
     * @param usuario
     * @return listado de modulos por usuario
     */
    public List<Modulo> obtenerListadoUsuarioModulo(Usuario usuario) {
        //// obtengo listado de roles del usuario
        List<UsuarioRol> usuarioRolList = usuarioRolFacade.obtenerRolUsuarioList(usuario.getIdUsuario());
        //// obtengo los modulos
        ArrayList<Modulo> moduloLista = new ArrayList<>();
        for (UsuarioRol usuarioRol : usuarioRolList) {
            List<RolModulo> rolModuloList = rolModuloFacade.obtenerRolModuloList(usuarioRol.getRol().getIdRol());
            for (RolModulo rolModulo : rolModuloList) {
                moduloLista.add(rolModulo.getModulo());
            }
        }

        return moduloLista;
    }

}
