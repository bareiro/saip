/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.UsuarioRol;
import entity.Rol;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author DESARROLLO
 */
@Stateless
public class UsuarioRolFacade extends AbstractFacade<UsuarioRol> {
	
	private final String QUERY_USUARIO_ROL = "select o from UsuarioRol o "
			+ "where o.estado in ('A') "
			+ "and o.usuario.idUsuario = :idUsuario "
			+ "and o.rol.idRol = :idRol";
	
	private final String QUERY_USUARIO_ROL_LISTA = "select o from UsuarioRol o "
			+ "where o.estado in ('A') "
			+ "and o.usuario.idUsuario = :idUsuario ";
	

    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioRolFacade() {
        super(UsuarioRol.class);
    }
    
    /**
     * Obtiene el rol y el usuario si existe
     * @param idUsuario
     * @param idRol
     * @return
     */
	public UsuarioRol obtenerRolUsuario(Integer idUsuario, Integer idRol){    	
    	try {
    		UsuarioRol usuarioRol = (UsuarioRol) em.createQuery(QUERY_USUARIO_ROL)
					.setParameter("idUsuario", idUsuario)
	    			.setParameter("idRol", idRol)
	    			.getSingleResult();
        	
        	return usuarioRol;
    	}catch(Exception e) {
    		return null;
    	}
    }
    
    @SuppressWarnings("unchecked")
	public List<UsuarioRol> obtenerRolUsuarioList(Integer idUsuario){    	
    	List<UsuarioRol> listaUsuarioRol = new ArrayList<>();
    	listaUsuarioRol = em.createQuery(QUERY_USUARIO_ROL_LISTA)
    							.setParameter("idUsuario", idUsuario)
				    			.getResultList();
    	return listaUsuarioRol;
    }
    

    public List<Rol> obtenerRolPorUsuario(Integer idUsuario){  
    	List<Rol> listaRol = new ArrayList<>();
    	//// obtiene el usuarioRol
    	List<UsuarioRol> listaUsuarioRol = obtenerRolUsuarioList(idUsuario);
    	if(listaUsuarioRol.isEmpty()) {
    		return listaRol;
    	}
    	
    	listaUsuarioRol.forEach((item) -> 
    		listaRol.add(item.getRol())
    	);
    	
    	return listaRol;
    }
    
}
