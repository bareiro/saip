/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import controller.Login;
import util.AES;

/**
 *
 */
@Stateless
public class LoginFacade extends AbstractFacade<Login> {

	private final String QUERY_CONTROL_CREDENCIALES = "select o from Usuario o "
			+ "where lower(o.usuario) = :user and o.contrasenha = :pass and estado = 'A' ";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Inject
    Login login;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LoginFacade() {
    	super(Login.class);
    }

    /**
     * Retorna los datos del usuario si las credenciales ingresadas son correctas
     * @param user
     * @param pass
     * @return
     */
    public Usuario controlCredenciales(String user, String pass) {
        try {
            Usuario usuario = em.createQuery(QUERY_CONTROL_CREDENCIALES, Usuario.class)
                    .setParameter("user", user)
                    .setParameter("pass", AES.hash(pass)).getSingleResult();
            return usuario;
        } catch (NoResultException ex) {
            return null;
        }

    }
    

    public boolean cambiarContrasenha(Usuario usuario, String pass) {
        try {

            em.createNativeQuery("update usuario "
                    + " set contrasenha = :v_contrasenha, usu_mod = :v_usu_mod, fecha_mod = :v_fecha_mod,"
                    + " primera = :v_primera"
                    + " where id_usuario = :v_id_usuario")
                    .setParameter("v_contrasenha", AES.hash(pass))
                    .setParameter("v_usu_mod", usuario.getUsuario())
                    .setParameter("v_fecha_mod", new Date())
                    .setParameter("v_id_usuario", usuario.getIdUsuario())
                    .setParameter("v_primera", "N")
                    .executeUpdate();
            return true;
        } catch (Exception ex) {

        }
        return false;
    }

    public String restablecerContrasenha(Usuario usuario) {
        try {
            Date hoy = new Date();
            usuario.setUsuMod(login.getUsuarioConectado().getUsuario());
            usuario.setFechaMod(hoy);
            usuario.setContrasenha(AES.hash("123456"));
            usuario.setPrimera("S");
            em.merge(usuario);

            em.flush();
            return "OK";
        } catch (PersistenceException ex) {
            return ex.toString();
        }
    }

    
    
}
