/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Pantalla;
import entity.RolPantalla;
import entity.UsuarioRol;
import entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

@Stateless
public class PantallaFacade extends AbstractFacade<Pantalla> {
	
	private final String QUERY_PANTALLA_LISTA_ACTIVO = "select o from Pantalla o where o.estado in ('A') ";
	private final String QUERY_PANTALLA_OBTENER = "select o from Pantallas o where o.pantalla = :pantalla";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @EJB
    UsuarioRolFacade usuarioRolFacade;
    @EJB
    RolPantallaFacade rolPantallaFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PantallaFacade() {
        super(Pantalla.class);
    }

    /**
     * obtiene la pantalla segun el campo pantalla
     * @param pantalla
     * @return
     */
    public Pantalla obtenerPantallas(String pantalla) {
        try {
            return (Pantalla) em.createQuery(QUERY_PANTALLA_OBTENER)
                    .setParameter("pantalla", pantalla)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException ex) {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<Pantalla> obtenerPantallaListActivos(){
    	List<Pantalla> listaPantalla = new ArrayList<>();
    	listaPantalla = em.createQuery(QUERY_PANTALLA_LISTA_ACTIVO).getResultList();
    	return listaPantalla;
    }

    /**
     * Se obtiene el listado de pantallas asociadas a un usuario
     * @param usuario
     * @return listado de pantallas por usuario
     */
    public List<Pantalla> obtenerListadoUsuarioPantalla(Usuario usuario) {
        //// obtengo listado de roles del usuario
        List<UsuarioRol> usuarioRolList = usuarioRolFacade.obtenerRolUsuarioList(usuario.getIdUsuario());
        //// obtengo las pantallas
        ArrayList<Pantalla> pantallaList = new ArrayList<>();
        for (UsuarioRol usuarioRol : usuarioRolList) {
            List<RolPantalla> rolPantallaList = rolPantallaFacade.obtenerRolPantallaList(usuarioRol.getRol().getIdRol());
            for (RolPantalla rolPantalla : rolPantallaList) {
                pantallaList.add(rolPantalla.getPantalla());
            }
        }

        return pantallaList;
    }
}
