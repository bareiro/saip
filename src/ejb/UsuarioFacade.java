/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

	private final String QUERY_USUARIO_LISTA_ACTIVO = "select o from Usuario o where o.estado in ('A') ";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    @SuppressWarnings("unchecked")
	public List<Usuario> obtenerUsuarioListActivos(){
    	List<Usuario> listaUsuario = new ArrayList<>();
    	listaUsuario = em.createQuery(QUERY_USUARIO_LISTA_ACTIVO).getResultList();
    	return listaUsuario;
    }
}
