/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Rol;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author DESARROLLO
 */
@Stateless
public class RolFacade extends AbstractFacade<Rol> {

	private final String QUERY_ROL_LISTA_ACTIVO = "select o from Rol o where o.estado in ('A') ";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolFacade() {
        super(Rol.class);
    }

    @SuppressWarnings("unchecked")
	public List<Rol> obtenerRolListActivos(){
    	List<Rol> listaRol = new ArrayList<>();
    	listaRol = em.createQuery(QUERY_ROL_LISTA_ACTIVO).getResultList();
    	return listaRol;
    }
    
}
