/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Pantalla;
import entity.RolPantalla;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author DESARROLLO
 */
@Stateless
public class RolPantallaFacade extends AbstractFacade<RolPantalla> {

	private final String QUERY_ROL_PANTALLA_LISTA = "select o from RolPantalla o "
			+ "where o.estado in ('A') "
			+ "and o.rol.idRol = :idRol ";
	
	private final String QUERY_ROL_PANTALLA_OBTENER = "select o from RolPantalla o "
			+ "where o.estado in ('A') "
			+ "and o.rol.idRol = :idRol "
			+ "and o.pantalla.idPantalla = :idPantalla ";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolPantallaFacade() {
        super(RolPantalla.class);
    }
    
    @SuppressWarnings("unchecked")
	public List<RolPantalla> obtenerRolPantallaList(Integer idRol){    	
    	List<RolPantalla> listaRolPantalla = new ArrayList<>();
    	listaRolPantalla = em.createQuery(QUERY_ROL_PANTALLA_LISTA)
    							.setParameter("idRol", idRol)
				    			.getResultList();
    	return listaRolPantalla;
    }
    
    public RolPantalla obtenerRolPantalla(Integer idRol, Integer idPantalla){    	
    	try {
	    	RolPantalla rolPantalla = (RolPantalla) em.createQuery(QUERY_ROL_PANTALLA_OBTENER)
	    							.setParameter("idRol", idRol)
	    							.setParameter("idPantalla", idPantalla)
					    			.getSingleResult();

	    	return rolPantalla;
    	}catch(Exception e) {
    		return null;
    	}
    }

    public List<Pantalla> obtenerPantallaPorRol(Integer idRol){  
    	List<Pantalla> listaPantalla = new ArrayList<>();
    	//// obtiene el rolPantalla
    	List<RolPantalla> listaRolPantalla = obtenerRolPantallaList(idRol);
    	if(listaRolPantalla.isEmpty()) {
    		return listaPantalla;
    	}
    	
    	listaRolPantalla.forEach((item) -> 
    		listaPantalla.add(item.getPantalla())
    	);
    	
    	return listaPantalla;
    }
    
}
