/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Persona;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class PersonaFacade extends AbstractFacade<Persona> {

	private final String QUERY_PERSONA_DOCUMENTO_IDENTIDAD = "select o from Persona o where documentoIdentidad = :documentoIdentidad";
	
    @PersistenceContext(unitName = "SaipPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonaFacade() {
        super(Persona.class);
    }

    public Persona obtenerPersonaDocumentoIdentidad(String documentoIdentidad) {
        try {
            Persona persona = em.createQuery(QUERY_PERSONA_DOCUMENTO_IDENTIDAD, Persona.class)
            		.setParameter("documentoIdentidad", documentoIdentidad).getSingleResult();
            return persona;
        } catch (NoResultException ex) {
            return null;
        }
    }    
}
