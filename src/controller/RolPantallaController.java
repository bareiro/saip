/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.PantallaFacade;
import ejb.RolPantallaFacade;
import ejb.RolFacade;
import entity.Pantalla;
import entity.RolPantalla;
import util.Constante;
import util.Util;
import entity.Rol;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@Named(value = "rolPantallaController")
@ViewScoped
public class RolPantallaController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    RolFacade rolesFacade;
    @EJB
    PantallaFacade pantallasFacade;
    @EJB
    private RolPantallaFacade rolPantallaFacade;
    @Inject
    Login login;

    private List<Pantalla> pantallaList;
    private List<Pantalla> pantallaPorRolList;

    private List<Rol> rolList;
    private String rolDescripcion;
    private Rol rolSeleccionado;    

    @PostConstruct
    public void init() {
        rolList = rolesFacade.obtenerRolListActivos();
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idRol");
        if (Util.isNotNullAndNotEmpty(id)) {
            pantallaList = pantallasFacade.obtenerPantallaListActivos();
        	rolSeleccionado = rolesFacade.find(Integer.valueOf(id));
        	rolDescripcion = rolSeleccionado.getDescripcion();
        	pantallaPorRolList = rolPantallaFacade.obtenerPantallaPorRol(rolSeleccionado.getIdRol());
        }
    }

    public String editar() {
        return "edit.xhtml";
    }

    public String volver() {
        return "list.xhtml";
    }

    public void eliminar(int idPantalla) {
        RolPantalla rolPantalla = rolPantallaFacade.obtenerRolPantalla(rolSeleccionado.getIdRol(), idPantalla);
        rolPantallaFacade.remove(rolPantalla);
        init();
        return;
    }
    
    public void agregar(int idPantalla) {
    	RolPantalla rolPantalla = rolPantallaFacade.obtenerRolPantalla(rolSeleccionado.getIdRol(), idPantalla);
        if(rolPantalla != null) {
        	init();
        	return;
        }    	
        rolPantalla = new RolPantalla();
        rolPantalla.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
        rolPantalla.setPantalla(pantallasFacade.find(idPantalla));
        rolPantalla.setRol(rolSeleccionado);
        rolPantalla.setUsuAlta(login.getUsuarioConectado().getUsuario());
        rolPantalla.setFechaAlta(new Date());
        rolPantallaFacade.create(rolPantalla);
        init();
        return;
    }

  
    /**
     * @return the rolList
     */
    public List<Rol> getRolList() {
        return rolList;
    }

    /**
     * @param rolList the rolList to set
     */
    public void setRolList(List<Rol> rolList) {
        this.rolList = rolList;
    }

    /**
     * @return the rolDescripcion
     */
    public String getRolDescripcion() {
        return rolDescripcion;
    }

    /**
     * @param rolDescripcion the rolDescripcion to set
     */
    public void setRolDescripcion(String rolDescripcion) {
        this.rolDescripcion = rolDescripcion;
    }

    public List<Pantalla> getPantallaList() {
        return pantallaList;
    }

    public void setPantallaList(List<Pantalla> pantallaList) {
        this.pantallaList = pantallaList;
    }

	public List<Pantalla> getPantallaPorRolList() {
		return pantallaPorRolList;
	}

	public void setPantallaPorRolList(List<Pantalla> pantallaPorRolList) {
		this.pantallaPorRolList = pantallaPorRolList;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}
    
}
