/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.ModuloFacade;
import ejb.RolModuloFacade;
import ejb.RolFacade;
import entity.Modulo;
import entity.RolModulo;
import entity.Rol;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
//import org.primefaces.context.RequestContext;

@Named(value = "rolModuloController")
@ViewScoped
public class RolModuloController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    RolFacade rolesFacade;

    @EJB
    private RolModuloFacade rolModuloFacade;
    @EJB
    private ModuloFacade moduloFacade;

    private Integer idRol;
    private Integer idModulo;
    private List<Modulo> moduloList;
    private String moduloSeleccionada;
    private List<Rol> rolesList;
    private boolean estado = true;
    private String descRol;
    private Rol rolSelect;
    private List<RolModulo> rolModuloList;
    private List<RolModulo> rolModuloListAux;
    private boolean habilitar;

    public void eliminarDetalle(int index) {
        rolModuloListAux.add(rolModuloList.get(index));
        rolModuloList.remove(index);
    }

    public void filtrarModulo() {
        //RequestContext context5 = RequestContext.getCurrentInstance();
        //context5.execute("PF('statusDialog').hide();");
        /*if (!descRol.equals("")) {
            rolesList = rolesFacade.getRolesListFill(descRol, "A");
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Roles Modulo", "Debe Ingresar algún filtro."));
        }*/
    }

    @PostConstruct
    public void init() {

        estado = true;
        setModuloList(moduloFacade.getModuloList(null, "A"));

        rolesList = new ArrayList<>();
        rolesList = rolesFacade.obtenerRolListActivos();

        descRol = null;
        rolSelect = new Rol();
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idRolModulo");
        if (id != null && !id.equals("")) {
            rolSelect = rolesFacade.find(Integer.valueOf(id));
            descRol = rolSelect.getDescripcion();
            rolModuloList = rolModuloFacade.obtenerRolModuloList(idRol);
            rolModuloListAux = new ArrayList<>();
        }

    }

    public String editar() {
        return "Edit";
    }

    public String actualizar(Integer id) {
        return "Edit";
    }

    public String volver() {
        return "List?faces-redirect=true";
    }

    public String actualizarRolModulo() {
        if (validar()) {

            String info = "";
            for (RolModulo o : rolModuloListAux) {
                rolModuloFacade.eliminar(o);
            }

            for (RolModulo o : rolModuloList) {
                o.setRol(rolSelect);
                o.setEstado("A");
                if (o.getIdRolModulo() == null) {
                    info = rolModuloFacade.guardar(o);
                } else {
                    info = rolModuloFacade.actualizar(o);
                }
            }
            if (!info.equals("OK")) {
                return null;
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Rol - Modulo", "Rol - Modulo Creado."));
                return "List";
            }
        } else {
            return null;
        }
    }

    public void agregarModulo() {
        if(moduloSeleccionada!=null && !"".equals(moduloSeleccionada)){
            Modulo modulo = moduloFacade.obtenerModulo(moduloSeleccionada);
            if(modulo!=null){
                RolModulo rolModulo = new RolModulo();
                rolModulo.setModulo(modulo);
                rolModuloList.add(rolModulo);
                idModulo = 0;
            }else{
                FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Rol - Modulo", "Debe Seleccionar un Modulo."));
            }            
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Rol - Modulo", "Debe Seleccionar un Modulo."));
        }
    }
    
    public boolean validar() {

        if (rolSelect == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Rol - Modulo", "Debe seleccionar el Rol."));
            return false;
        }

        if (rolModuloList.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Rol - Modulo", "Debe ingresar al menos un Modulo."));
            return false;
        }
        return true;
    }

    
    public List<String> listModuloFiltro(String query) {
	List<String> moduloListFiltro = new ArrayList<>();
        
        for (int i = 0; i < moduloList.size(); i++) {
            Modulo modulo = moduloList.get(i);
            if(modulo.getDescripcion().toLowerCase().contains(query.toLowerCase())) {
                moduloListFiltro.add(modulo.getDescripcion());
            }
        }        
        return moduloListFiltro;
    }

    /**
     * @return the idRol
     */
    public Integer getIdRol() {
        return idRol;
    }

    /**
     * @param idRol the idRol to set
     */
    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }


    /**
     * @return the estado
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    /**
     * @return the habilitar
     */
    public boolean isHabilitar() {
        return habilitar;
    }

    /**
     * @param habilitar the habilitar to set
     */
    public void setHabilitar(boolean habilitar) {
        this.habilitar = habilitar;
    }

    /**
     * @return the rolesList
     */
    public List<Rol> getRolesList() {
        return rolesList;
    }

    /**
     * @param rolesList the rolesList to set
     */
    public void setRolesList(List<Rol> rolesList) {
        this.rolesList = rolesList;
    }

    /**
     * @return the moduloList
     */
    public List<Modulo> getModuloList() {
        return moduloList;
    }

    /**
     * @param moduloList the moduloList to set
     */
    public void setModuloList(List<Modulo> moduloList) {
        this.moduloList = moduloList;
    }

    /**
     * @return the descRol
     */
    public String getDescRol() {
        return descRol;
    }

    /**
     * @param descRol the descRol to set
     */
    public void setDescRol(String descRol) {
        this.descRol = descRol;
    }

    /**
     * @return the idModulo
     */
    public Integer getIdModulo() {
        return idModulo;
    }

    /**
     * @param idModulo the idModulo to set
     */
    public void setIdModulo(Integer idModulo) {
        this.idModulo = idModulo;
    }

    /**
     * @return the rolModuloList
     */
    public List<RolModulo> getRolModuloList() {
        return rolModuloList;
    }

    /**
     * @param rolModuloList the rolModuloList to set
     */
    public void setRolModuloList(List<RolModulo> rolModuloList) {
        this.rolModuloList = rolModuloList;
    }

    /**
     * @return the rolModuloListAux
     */
    public List<RolModulo> getRolModuloListAux() {
        return rolModuloListAux;
    }

    /**
     * @param rolModuloListAux the rolModuloListAux to set
     */
    public void setRolModuloListAux(List<RolModulo> rolModuloListAux) {
        this.rolModuloListAux = rolModuloListAux;
    }

    public String getModuloSeleccionada() {
        return moduloSeleccionada;
    }

    public void setModuloSeleccionada(String moduloSeleccionada) {
        this.moduloSeleccionada = moduloSeleccionada;
    }
    
    

}
