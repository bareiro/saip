/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ejb.RolFacade;
import entity.Rol;
import util.Constante;
import util.Util;

@Named(value = "rolController")
@ViewScoped
public class RolController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    RolFacade rolFacade;
	@Inject
    Login login;

    private List<Rol> rolesList;
    Rol rol;
    private boolean estado = true;
    private String descripcion;
    private final String entidad = "Rol";

    @PostConstruct
    public void init() {
        setRolesList(rolFacade.findAll());

        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idRol");
        if (Util.isNotNullAndNotEmpty(id)) {
            rol = rolFacade.find(Integer.valueOf(id));
            descripcion = rol.getDescripcion();
            setEstado(rol.getEstado().equals(Constante.ESTADO_REGISTRO_ACTIVO));
        }
    }
    	
    public String crear() {
        return "create.xhtml";
    }

    public String editar() {
        return "edit.xhtml";
    }

    public String guardar() {
    	if (!validar()) {
    		return null;
    	}
        try {
        	rol = new Rol();
            rol.setDescripcion(descripcion.trim().toUpperCase()); 
            rol.setUsuAlta(login.getUsuarioConectado().getUsuario());
            rol.setFechaAlta(new Date());
            rol.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
        	rolFacade.create(rol);    	
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
            		Util.obtenerMensajeSistema("DatosCreado","Rol")));
            return "list.xhtml";
        }
        catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
        	return null;
        } 
    }

    public String actualizar() {
    	if (!validar()) {
    		return null;
        }
        try {
	        rol.setDescripcion(descripcion.trim().toUpperCase());
	        if(estado==true){rol.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);}else{rol.setEstado(Constante.ESTADO_REGISTRO_INACTIVO);}	
	        rol.setFechaMod(new Date());
	        rol.setUsuMod(login.getUsuarioConectado().getUsuario());
	        rolFacade.edit(rol);
	        FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
	        		Util.obtenerMensajeSistema("DatosActualizada","Rol")));
	        return "list.xhtml";
        }catch(Exception e) {
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
        	return null;
        }       
    }

    public boolean validar() {
        if (!Util.isNotNullAndNotEmpty(descripcion)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","descripcion")));
            return false;
        }
        return true;
    }

    public String volver() {
        return "List.xhtml";
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the rolesList
     */
    public List<Rol> getRolesList() {
        return rolesList;
    }

    /**
     * @param rolesList the rolesList to set
     */
    public void setRolesList(List<Rol> rolesList) {
        this.rolesList = rolesList;
    }

    /**
     * @return the estado
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

}
