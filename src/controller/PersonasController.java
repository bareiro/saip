/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.PersonaFacade;
import entity.Persona;
import util.Constante;
import util.Util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 */
@Named(value = "personaController")
@ViewScoped
public class PersonasController implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String entidad = "Persona";
	@EJB
    PersonaFacade personaFacade;
	@Inject
    Login login;
    private Boolean estado;
    private Persona persona;
    
    private String nombreApellidoFiltro;

    private String documentoIdentidad;
    private String nombreApellido;
    private String sexo;
    private String fechaNacimiento;
    private String telefono;
    private String direccion;
    private String correoElectronico;
    private String lugarNacimiento;

    private List<Persona> personasList;
    
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    public PersonasController() {
    }

    @PostConstruct
    public void init() {
        estado = true;
        personasList = personaFacade.findAll();

        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idPersona");
        if (Util.isNotNullAndNotEmpty(id)) {
            persona = personaFacade.find(Long.valueOf(id));
            nombreApellido = persona.getNombreApellido();
            documentoIdentidad = persona.getDocumentoIdentidad();
            fechaNacimiento = (persona.getFechaNacimiento()!=null?formatoFecha.format(persona.getFechaNacimiento()):null);
            sexo = persona.getSexo();
            correoElectronico = persona.getCorreoElectronico();
            telefono = persona.getTelefono();
            direccion = persona.getDireccion();
            setEstado(persona.getEstado().equals(Constante.ESTADO_REGISTRO_ACTIVO));
        }

    }

    public String crear() {
        return "create.xhtml";
    }
    
    public String editar() {
        return "edit.xhtml";
    }

    public String volver() {
        return "list.xhtml";
    }

    public String guardar() {
    	if (!validar()) {
    		return null;
    	}
    	try {
            persona = new Persona();
            persona.setNombreApellido(nombreApellido.trim().toUpperCase());
            persona.setDocumentoIdentidad(documentoIdentidad.trim().toUpperCase());
            persona.setFechaNacimiento((Util.isNotNullAndNotEmpty(fechaNacimiento)?formatoFecha.parse(fechaNacimiento):null));
            persona.setCorreoElectronico(correoElectronico);
            persona.setDireccion(direccion);
            persona.setTelefono(telefono);
            persona.setSexo(sexo);
            persona.setFechaAlta(new Date());
        	persona.setUsuAlta(login.getUsuarioConectado().getUsuario());
            persona.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
            personaFacade.create(persona);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
            		Util.obtenerMensajeSistema("DatosCreado","Persona")));
            return "list.xhtml";

        }catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
        	return null;
        } 

    }

    public boolean validar() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!Util.isNotNullAndNotEmpty(nombreApellido)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","Nombre y/o Apellido")));
            return false;
        }
        if (!Util.isNotNullAndNotEmpty(documentoIdentidad)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","Documento de identidad")));
            return false;
        }        
        if (!Util.isNotNullAndNotEmpty(sexo)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","Sexo")));
            return false;
        }
        if (Util.isNotNullAndNotEmpty(fechaNacimiento)) {
        	try {
				formatoFecha.parse(fechaNacimiento);
			} catch (ParseException e) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
	            		Util.obtenerMensajeSistema("FormFormatoFecha","fechaNacimiento")));
	            return false;
			}            
        }

        return true;
    }

    public String actualizar() {
    	if (!validar()) {
    		return null;
    	}
    	try {
            persona.setNombreApellido(nombreApellido.trim().toUpperCase());
            persona.setDocumentoIdentidad(documentoIdentidad.trim().toUpperCase());
            persona.setFechaNacimiento((Util.isNotNullAndNotEmpty(fechaNacimiento)?formatoFecha.parse(fechaNacimiento):null));
            persona.setCorreoElectronico(correoElectronico);
            persona.setDireccion(direccion);
            persona.setTelefono(telefono);
            persona.setSexo(getSexo());
            persona.setFechaMod(new Date());
        	persona.setUsuMod(login.getUsuarioConectado().getUsuario());
            if (getEstado()) {
                persona.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
            } else {
                persona.setEstado(Constante.ESTADO_REGISTRO_INACTIVO);
            }
            personaFacade.edit(persona);
            FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
	        		Util.obtenerMensajeSistema("DatosActualizada","Persona")));
	        return "list.xhtml";

    	}catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
        	return null;
        } 
    }

    /**
     * @return the estado
     */
    public Boolean getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * @return the nombreApellidoFiltro
     */
    public String getNombreApellidoFiltro() {
        return nombreApellidoFiltro;
    }

    /**
     * @param nombreApellidoFiltro the nombreApellidoFiltro to set
     */
    public void setNombreApellidoFiltro(String nombreApellidoFiltro) {
        this.nombreApellidoFiltro = nombreApellidoFiltro;
    }

    /**
     * @return the documentoIdentidad
     */
    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    /**
     * @param documentoIdentidad the documentoIdentidad to set
     */
    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    /**
     * @return the nombreApellido
     */
    public String getNombreApellido() {
        return nombreApellido;
    }

    /**
     * @param nombreApellido the nombreApellido to set
     */
    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    /**
     * @return the fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the personasList
     */
    public List<Persona> getPersonasList() {
        return personasList;
    }

    /**
     * @param personasList the personasList to set
     */
    public void setPersonasList(List<Persona> personasList) {
        this.personasList = personasList;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the correoElectronico
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * @param correoElectronico the correoElectronico to set
     */
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    /**
     * @return the lugarNacimiento
     */
    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    /**
     * @param lugarNacimiento the lugarNacimiento to set
     */
    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

}
