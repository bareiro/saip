/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;

/**
 *
 * @author desaExcelsis
 */
@Named(value = "logoutController")
@Dependent
public class LogoutController {

    /**
     * Creates a new instance of LogoutController
     */
    public LogoutController() {
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login";

    }
}
