/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.LoginFacade;
import ejb.ModuloFacade;
import ejb.PantallaFacade;
import ejb.UsuarioFacade;
import entity.Modulo;
import entity.Pantalla;
import entity.Usuario;
import util.Constante;
import util.Util;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Named(value = "login")
@SessionScoped
public class Login implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
    UsuarioFacade usuarioFacade;
	@EJB
    LoginFacade loginFacade;
    @EJB
    PantallaFacade pantallaFacade;
    @EJB
    ModuloFacade moduloFacade;

    private String user;
    private String pass;
    private Usuario usuarioConectado;
    private List<Modulo> moduloList;
    private List<Pantalla> pantallaList;

    /**
     * Creates a new instance of Login
     */
    public Login() {}

    public void login() throws IOException {
    	final String primerLogin = Constante.ESTADO_USUARIO_NUEVO;
    	
    	FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        //// control de parametros
        if(!Util.isNotNullAndNotEmpty(this.user) || !Util.isNotNullAndNotEmpty(this.pass)) {
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atenci�n", 
        			Util.obtenerMensajeSistema("LoginParametrosRequeridos")));
        	return;
        }
        
        /*try {
            request.login(this.user.trim().toLowerCase(), this.pass);*/

            usuarioConectado = loginFacade.controlCredenciales(this.user.trim().toLowerCase(), this.pass);
            if (usuarioConectado != null) {
                //// obtengo listado de pantallas permitidas por usuario
                HttpSession session = request.getSession(true);
                pantallaList = pantallaFacade.obtenerListadoUsuarioPantalla(usuarioConectado);
                session.setAttribute("pantallas", pantallaList);
                moduloList = moduloFacade.obtenerListadoUsuarioModulo(usuarioConectado);

                if (primerLogin.equals(usuarioConectado.getPrimera())) {
                	externalContext.redirect(externalContext.getRequestContextPath() + "/pages/administracion/usuario/cambiar_contrasenha.xhtml");
                } else {
                    session.setAttribute("authenticated", true);
                    externalContext.redirect(externalContext.getRequestContextPath() + "/pages/index.xhtml");
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atenci�n", 
                		Util.obtenerMensajeSistema("LoginIncorrecto")));
            }
        /*} catch (ServletException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atenci�n",
            		Util.obtenerRecursoSistema(Constante.bundleMensajeSistema, "LoginIncorrecto")));
        }*/
    }

    public void logout() {
    	FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        limpiarDatosLogin();
        try {
            request.logout();
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            externalContext.redirect(externalContext.getRequestContextPath() + "/pages/login.xhtml");
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Atenci�n", 
            		Util.obtenerMensajeSistema("LogoutError", e.getMessage())));
        } catch (IOException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Atenci�n", 
            		Util.obtenerMensajeSistema("LogoutError", e.getMessage())));
		}
    }

    public void clearCacheWildFly() {
        try {
            ObjectName jaasMgr = new ObjectName("jboss.as:subsystem=security,security-domain=sdSaip");
            List<String> lista = new ArrayList<String>();
            lista.add(usuarioConectado.getUsuario());
            Object[] params = lista.toArray();
            if (params == null) {
                return;
            }
            String[] signature = {"java.lang.String"};
            MBeanServer server = (MBeanServer) MBeanServerFactory.findMBeanServer(null).get(0);
            server.invoke(jaasMgr, "flushCache", params, signature);
        } catch (InstanceNotFoundException | MBeanException | MalformedObjectNameException | ReflectionException ex) {
            ex.printStackTrace();
        }
    }
    
    private void limpiarDatosLogin() {
    	clearCacheWildFly();
        usuarioConectado = null;
        user = null;
        pass = null;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the usuarioConectado
     */
    public Usuario getUsuarioConectado() {
        return usuarioConectado;
    }


    /**
     * @author mbareiro verifica si tiene permiso a nivel pantalla
     * @param pantalla
     * @return
     */
    public boolean tienePermisoPantalla(String pantalla) {
        if (pantallaList != null) {
            for (Pantalla permisoPantalla : pantallaList) {
                if (permisoPantalla.getDescripcion().toLowerCase().contains(pantalla.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @author mbareiro verifica si tiene permiso a nivel de modulos
     * @param modulo
     * @return
     */
    public boolean tienePermisoModulo(String modulo) {
        if (moduloList != null) {
            for (Modulo permisoModulo : moduloList) {
                if (modulo.toLowerCase().contains(permisoModulo.getDescripcion().toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

}
