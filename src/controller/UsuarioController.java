/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.LoginFacade;
import ejb.PersonaFacade;
import ejb.UsuarioFacade;
import entity.Persona;
import entity.Usuario;
import util.AES;
import util.Constante;
import util.Util;

import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@Named(value = "usuarioController")
@ViewScoped
public class UsuarioController implements Serializable {

	private static final long serialVersionUID = 1L;
    private List<Usuario> usuarioList;
    private boolean estado = true;
    private String email;
    private Persona persona;
    private String usuarioIden;
    private Usuario usuario;
    private final String entidad = "Usuario";
    private String documentoIdentidad;
    //// cambio de contraseņa
    private String password1;
    private String password2;

    @Inject
    Login login;

    @EJB
    private UsuarioFacade usuarioFacade;
    @EJB
    private PersonaFacade personaFacade;
    
    @EJB
    private LoginFacade loginFacade;


    @PostConstruct
    public void init() {
        setUsuarioList(usuarioFacade.findAll());
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idUsuario");
        if (Util.isNotNullAndNotEmpty(id)) {
            usuario = usuarioFacade.find(Integer.valueOf(id));
        	usuarioIden = usuario.getUsuario();
            estado = usuario.getEstado().equals(Constante.ESTADO_REGISTRO_ACTIVO);
            email = usuario.getEmail();
            persona = usuario.getPersona();
        }
    }

    public String crear() {
        return "create.xhtml";
    }

    public String editar() {
        return "edit.xhtml";
    }

    public String volver() {
        return "list.xhtml";
    }
    
    public void buscarPersona() {
    	if(Util.isNotNullAndNotEmpty(documentoIdentidad)) {
        	persona = personaFacade.obtenerPersonaDocumentoIdentidad(documentoIdentidad);
        	if(persona == null) {
        		FacesContext context = FacesContext.getCurrentInstance();
            	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
                		Util.obtenerMensajeSistema("DatosNoEncontrado",documentoIdentidad)));
        	}
    	}  
    }    
    
    public String guardar() {
    	if (!validar()) {
    		return null;
    	}
    	try {
    		Usuario usuario = new Usuario();
    		usuario.setPersona(persona);
            usuario.setUsuario(usuarioIden.trim().toLowerCase());
            usuario.setContrasenha(AES.hash(Util.obtenerParametroSistema("PassInicial")));
            usuario.setEmail(email);
            usuario.setPrimera(Constante.ESTADO_USUARIO_NUEVO);
            usuario.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
            usuario.setUsuAlta(login.getUsuarioConectado().getUsuario());
            usuario.setFechaAlta(new Date());
            usuarioFacade.create(usuario);
            FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
            		Util.obtenerMensajeSistema("DatosCreado","Usuario")));
            return "list.xhtml";
        } catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
           
        	return null;
        } 
    }

    public String actualizar() {
    	if (!validar()) {
    		return null;
    	}
    	try {
    		usuario.setPersona(persona);
    		if(estado==true){usuario.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);}else{usuario.setEstado(Constante.ESTADO_REGISTRO_INACTIVO);}
            usuario.setUsuario(usuarioIden.trim().toLowerCase());
            usuario.setEmail(email);
            usuario.setUsuMod(login.getUsuarioConectado().getUsuario());
            usuario.setFechaMod(new Date());
            usuarioFacade.edit(usuario);
            FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
	        		Util.obtenerMensajeSistema("DatosActualizada","Usuario")));
	        return "list.xhtml";
        } catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
           
        	return null;
        } 
    }

    public boolean validar() {        
        if (!Util.isNotNullAndNotEmpty(usuarioIden)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","usuario")));
            return false;
        }
        if (persona==null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","persona")));
            return false;
        }
        return true;
    }
    
    public String restablecerContrasenha() {
    	String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idUsuario");
        Usuario usuario = usuarioFacade.find(Integer.parseInt(id));
        usuario.setContrasenha(AES.hash(Util.obtenerParametroSistema("PassInicial")));
        usuario.setPrimera(Constante.ESTADO_USUARIO_NUEVO);
        usuarioFacade.edit(usuario);       
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
        		Util.obtenerMensajeSistema("ContrasenhaActualizada")));
        return "list.xhtml";
    }
    
    public String cambiarContrasenha() {
    	if(!Util.isNotNullAndNotEmpty(password1) || !Util.isNotNullAndNotEmpty(password2)) {
    		FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","contraseņa")));
            return null;
    	}
    	if(!password1.equals(password2)) {
    		FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("ContrasenhaNoCoinciden")));
            return null;
    	}    	
    	    	
    	Usuario usuario = login.getUsuarioConectado();
        usuario.setContrasenha(AES.hash(password1));
        usuario.setPrimera(Constante.ESTADO_USUARIO_VIEJO);
        usuarioFacade.edit(usuario);       
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
        		Util.obtenerMensajeSistema("DatosActualizada","Contraseņa")));
        return null;
    }

    /**
     * @return the usuarioList
     */
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    /**
     * @param usuariosList the usuarioList to set
     */
    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    /**
     * @return the usuarioFacade
     */
    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    /**
     * @param usuarioFacade the usuarioFacade to set
     */
    public void setUsuarioFacade(UsuarioFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public String getUsuarioIden() {
		return usuarioIden;
	}

	public void setUsuarioIden(String usuarioIden) {
		this.usuarioIden = usuarioIden;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
     * @return the estado
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    /**
     * @return the documentoIdentidad
     */
    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    /**
     * @param cedulaIdentidad the documentoIdentidad to set
     */
    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

    
    
}
