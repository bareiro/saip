/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.PantallaFacade;
import entity.Pantalla;
import util.Constante;
import util.Util;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import javax.faces.view.ViewScoped;

@Named(value = "pantallaController")
@ViewScoped
public class PantallaController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    PantallaFacade pantallaFacade;	
	@Inject
    Login login;

    private String pantallaDesc;
    private boolean estado;
    Pantalla pantalla;
    private List<Pantalla> pantallasList;
    private final String entidad = "Pantalla";
    
    public PantallaController() {
    }

    @PostConstruct
    public void init() {
        pantallasList = pantallaFacade.findAll();
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idPantalla");
        if (Util.isNotNullAndNotEmpty(id)) {
            pantalla = pantallaFacade.find(Integer.valueOf(id));
            setPantallaDesc(pantalla.getDescripcion().toUpperCase());
            setEstado(pantalla.getEstado().equals(Constante.ESTADO_REGISTRO_ACTIVO));
        }
    }

    public String crear() {
        return "create.xhtml";
    }

    public String editar() {
    	return "edit.xhtml";
    }

    public String guardar() {
    	if (!validar()) {
    		return null;
    	}
        try {
            pantalla = new Pantalla();
            pantalla.setDescripcion(pantallaDesc.trim().toUpperCase());
            pantalla.setUsuAlta(login.getUsuarioConectado().getUsuario());
            pantalla.setFechaAlta(new Date());
            pantalla.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
        	pantallaFacade.create(pantalla);    	
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
            		Util.obtenerMensajeSistema("DatosCreado","Pantalla")));
            return "list.xhtml";
        }
        catch(Exception e) {  
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
           
        	return null;
        } 
    }

    public String actualizar() {
        if (!validar()) {
        	return null;
        }
        try {
	        pantalla.setDescripcion(pantallaDesc.trim().toUpperCase());
	        if(estado==true){pantalla.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);}else{pantalla.setEstado(Constante.ESTADO_REGISTRO_INACTIVO);}
	        pantalla.setFechaMod(new Date());
	        pantalla.setUsuMod(login.getUsuarioConectado().getUsuario());
	        pantallaFacade.edit(pantalla);
	        FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, entidad, 
	        		Util.obtenerMensajeSistema("DatosActualizada","Pantalla")));
	        return "list.xhtml";
        }catch(Exception e) {
        	FacesContext context = FacesContext.getCurrentInstance();
        	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, entidad, 
            		Util.obtenerMensajeSistema("DatosError",e.getMessage())));
        	return null;
        }
    }

    private boolean validar() {
        if (!Util.isNotNullAndNotEmpty(pantallaDesc)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, entidad, 
            		Util.obtenerMensajeSistema("FormCampoRequerido","descripcion")));
            return false;
        }
        return true;
    }

    public String volver() {
        return "list.xhtml";
    }

    /**
     * @return the pantallasList
     */
    public List<Pantalla> getPantallasList() {
        return pantallasList;
    }

    /**
     * @param pantallasList the pantallasList to set
     */
    public void setPantallasList(List<Pantalla> pantallasList) {
        this.pantallasList = pantallasList;
    }

    /**
     * @return the pantalla
     */
    public String getPantallaDesc() {
        return pantallaDesc;
    }

    /**
     * @param pantallaDesc
     */
    public void setPantallaDesc(String pantallaDesc) {
        this.pantallaDesc = pantallaDesc;
    }

    /**
     * @return the estado
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
