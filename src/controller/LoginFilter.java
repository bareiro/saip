/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Pantalla;
import util.Util;

import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession();

        //// ------------- mbareiro: manejo de permiso por pantalla ------------- ////
        //// obtengo la pagina a visitar
        String url = req.getRequestURI();    

        if(url.contains("sin_permiso.xhtml")) {
        	chain.doFilter(request, response);
        	return;
        }
        
        //// control de permisos
        if(url.contains("/pages/")) {
        	if(url.contains("login.xhtml")){
        		if(session.getAttribute("authenticated") != null){          		
	        		res.sendRedirect(req.getContextPath() + "/pages/index.xhtml");
	            	return;  
        		}else {
        			chain.doFilter(request, response);
        			return;
        		}
        	}
        	//// control de permiso por pantalla
        	while (url.contains("//")) {
                url = url.replace("//", "/");
            }
            //// obtengo el listado de pantallas de la session
            List<Pantalla> pantallaList = (List<Pantalla>) session.getAttribute("pantallas");
            //// consulto si el usuario tiene permiso a la pantalla solicitada
            boolean permisoPantalla = tienePermisoPantalla(url, pantallaList);
            //// no le deja pasar o si
            if(permisoPantalla==false){
                res.sendRedirect(req.getContextPath() + "/pages/sin_permiso.xhtml");
                return;
            } 
        	chain.doFilter(request, response);
            return;
        }else {
        	if(url.endsWith("/Saip/")) {
        		if (session.getAttribute("authenticated") != null){
        			res.sendRedirect(req.getContextPath() + "/pages/index.xhtml");
	            	return;
        		}
        	}
        	
        	chain.doFilter(request, response);
        }

    }
    
    /**
     * @author mbareiro
     * Comprueba si la url solicitada esta con permiso de accederse
     * @param url
     * @param pantallaList
     * @return 
     */
    private boolean tienePermisoPantalla(String url, List<Pantalla> pantallaList){       
        boolean tienePermiso = false;
        
        if(pantallaList == null || !Util.isNotNullAndNotEmpty(url)) {
        	return tienePermiso;
        }
        
        for(Pantalla pantalla : pantallaList){
            if(url.toLowerCase().contains(pantalla.getDescripcion().toLowerCase())){
                tienePermiso = true;
                break;
            }
        }
        
        return tienePermiso;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
