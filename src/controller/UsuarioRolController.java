/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.RolFacade;
import ejb.UsuarioRolFacade;
import ejb.UsuarioFacade;
import entity.UsuarioRol;
import util.Constante;
import util.Util;
import entity.Rol;
import entity.Usuario;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;


/**
 *
 */
@Named(value = "usuarioRolController")
@ViewScoped
public class UsuarioRolController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    RolFacade rolFacade;
    @EJB
    UsuarioFacade usuarioFacade;
    @EJB
    private UsuarioRolFacade usuarioRolFacade;
    @Inject
    Login login;
    
    private List<Usuario> usuarioList;
    private List<Rol> rolPorUsuarioList;
    private List<Rol> rolList;
    private String usuarioDescripcion;
    private Usuario usuarioSeleccionado;


    public String editar() {
        return "edit.xhtml";
    }

    public String volver() {
        return "list.xhtml";
    }

    @PostConstruct
    public void init() {
    	usuarioList = usuarioFacade.obtenerUsuarioListActivos();
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idUsuario");
        if (Util.isNotNullAndNotEmpty(id)) {
        	rolList = rolFacade.obtenerRolListActivos();
            usuarioSeleccionado = usuarioFacade.find(Integer.valueOf(id));
            usuarioDescripcion = usuarioSeleccionado.getUsuario();
            rolPorUsuarioList = usuarioRolFacade.obtenerRolPorUsuario(usuarioSeleccionado.getIdUsuario());
        }
    }
    
    public void eliminar(int idRol) {
        UsuarioRol usuarioRol = usuarioRolFacade.obtenerRolUsuario(usuarioSeleccionado.getIdUsuario(), idRol);
        usuarioRolFacade.remove(usuarioRol);
        init();
        return;
    }
    
    public void agregar(int idRol) {
    	UsuarioRol usuarioRol = usuarioRolFacade.obtenerRolUsuario(usuarioSeleccionado.getIdUsuario(), idRol);
        if(usuarioRol != null) {
        	init();
        	return;
        }    	
        usuarioRol = new UsuarioRol();
        usuarioRol.setEstado(Constante.ESTADO_REGISTRO_ACTIVO);
        usuarioRol.setRol(rolFacade.find(idRol));
        usuarioRol.setUsuario(usuarioSeleccionado);
        usuarioRol.setUsuAlta(login.getUsuarioConectado().getUsuario());
        usuarioRol.setFechaAlta(new Date());
        usuarioRolFacade.create(usuarioRol);
        init();
        return;
    }


    /**
     * @return the usuarioList
     */
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    /**
     * @param usuarioList the usuarioList to set
     */
    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    /**
     * @return the usu
     */
	public String getUsuarioDescripcion() {
		return usuarioDescripcion;
	}

	public void setUsuarioDescripcion(String usuarioDescripcion) {
		this.usuarioDescripcion = usuarioDescripcion;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public List<Rol> getRolList() {
		return rolList;
	}

	public void setRolList(List<Rol> rolList) {
		this.rolList = rolList;
	}

	public List<Rol> getRolPorUsuarioList() {
		return rolPorUsuarioList;
	}

	public void setRolPorUsuarioList(List<Rol> rolPorUsuarioList) {
		this.rolPorUsuarioList = rolPorUsuarioList;
	}
    
}
