/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DESARROLLO
 */
@Entity
@Table(name = "rol_pantalla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolPantalla.findAll", query = "SELECT r FROM RolPantalla r"),
    @NamedQuery(name = "RolPantalla.findByIdRolPantalla", query = "SELECT r FROM RolPantalla r WHERE r.idRolPantalla = :idRolPantalla"),
    @NamedQuery(name = "RolPantalla.findByEstado", query = "SELECT r FROM RolPantalla r WHERE r.estado = :estado"),
    @NamedQuery(name = "RolPantalla.findByUsuAlta", query = "SELECT r FROM RolPantalla r WHERE r.usuAlta = :usuAlta"),
    @NamedQuery(name = "RolPantalla.findByFechaAlta", query = "SELECT r FROM RolPantalla r WHERE r.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "RolPantalla.findByUsuMod", query = "SELECT r FROM RolPantalla r WHERE r.usuMod = :usuMod"),
    @NamedQuery(name = "RolPantalla.findByFechaMod", query = "SELECT r FROM RolPantalla r WHERE r.fechaMod = :fechaMod")})
public class RolPantalla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_rol_pantalla")
    private Integer idRolPantalla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "usu_alta")
    private String usuAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Size(max = 20)
    @Column(name = "usu_mod")
    private String usuMod;
    @Column(name = "fecha_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMod;
    @JoinColumn(name = "id_pantalla", referencedColumnName = "id_pantalla")
    @ManyToOne(optional = false)
    private Pantalla pantalla;
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
    @ManyToOne(optional = false)
    private Rol rol;

    public RolPantalla() {
    }

    public RolPantalla(Integer idRolPantalla) {
        this.idRolPantalla = idRolPantalla;
    }

    public RolPantalla(Integer idRolPantalla, String estado, String usuAlta, Date fechaAlta) {
        this.idRolPantalla = idRolPantalla;
        this.estado = estado;
        this.usuAlta = usuAlta;
        this.fechaAlta = fechaAlta;
    }

    public Integer getIdRolPantalla() {
        return idRolPantalla;
    }

    public void setIdRolPantalla(Integer idRolPantalla) {
        this.idRolPantalla = idRolPantalla;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Pantalla getPantalla() {
        return pantalla;
    }

    public void setPantalla(Pantalla pantalla) {
        this.pantalla = pantalla;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRolPantalla != null ? idRolPantalla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolPantalla)) {
            return false;
        }
        RolPantalla other = (RolPantalla) object;
        if ((this.idRolPantalla == null && other.idRolPantalla != null) || (this.idRolPantalla != null && !this.idRolPantalla.equals(other.idRolPantalla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RolPantalla[ idRolPantalla=" + idRolPantalla + " ]";
    }
    
}
