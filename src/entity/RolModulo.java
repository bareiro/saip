/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "rol_modulo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolModulo.findAll", query = "SELECT r FROM RolModulo r"),
    @NamedQuery(name = "RolModulo.findByIdRolModulo", query = "SELECT r FROM RolModulo r WHERE r.idRolModulo = :idRolModulo"),
    @NamedQuery(name = "RolModulo.findByEstado", query = "SELECT r FROM RolModulo r WHERE r.estado = :estado"),
    @NamedQuery(name = "RolModulo.findByUsuAlta", query = "SELECT r FROM RolModulo r WHERE r.usuAlta = :usuAlta"),
    @NamedQuery(name = "RolModulo.findByFechaAlta", query = "SELECT r FROM RolModulo r WHERE r.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "RolModulo.findByUsuMod", query = "SELECT r FROM RolModulo r WHERE r.usuMod = :usuMod"),
    @NamedQuery(name = "RolModulo.findByFechaMod", query = "SELECT r FROM RolModulo r WHERE r.fechaMod = :fechaMod")})
public class RolModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_rol_modulo")
    private Integer idRolModulo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "usu_alta")
    private String usuAlta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Size(max = 20)
    @Column(name = "usu_mod")
    private String usuMod;
    @Column(name = "fecha_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMod;
    @JoinColumn(name = "id_modulo", referencedColumnName = "id_modulo")
    @ManyToOne(optional = false)
    private Modulo modulo;
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
    @ManyToOne(optional = false)
    private Rol rol;

    public RolModulo() {
    }

    public RolModulo(Integer idRolModulo) {
        this.idRolModulo = idRolModulo;
    }

    public RolModulo(Integer idRolModulo, String estado, String usuAlta, Date fechaAlta) {
        this.idRolModulo = idRolModulo;
        this.estado = estado;
        this.usuAlta = usuAlta;
        this.fechaAlta = fechaAlta;
    }

    public Integer getIdRolModulo() {
        return idRolModulo;
    }

    public void setIdRolModulo(Integer idRolModulo) {
        this.idRolModulo = idRolModulo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRolModulo != null ? idRolModulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolModulo)) {
            return false;
        }
        RolModulo other = (RolModulo) object;
        if ((this.idRolModulo == null && other.idRolModulo != null) || (this.idRolModulo != null && !this.idRolModulo.equals(other.idRolModulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RolModulo[ idRolModulo=" + idRolModulo + " ]";
    }
    
}
