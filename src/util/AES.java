/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class AES {

    static String IV = "AAAAAAAAAAAAAAAA";
    static String plaintext = "12345678910112";

    static String encryptionKey = "P4trull4C4min3r4.key";

    public static void iniciarEncriptacion() {
        try {

            System.out.println("==Java==");
            System.out.println("plain:   " + plaintext);

            byte[] cipher = encrypt(plaintext, encryptionKey);

            System.out.print("cipher:  ");
            for (int i = 0; i < cipher.length; i++) {
                System.out.print(new Integer(cipher[i]) + " ");
            }
            System.out.println("");

            String decrypted = decrypt(cipher, encryptionKey);

            System.out.println("decrypt: " + decrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void iniciarDesencriptacion() {
        try {

            System.out.println("==Java==");
            System.out.println("plain:   " + plaintext);

            String cipher = encrypt2(plaintext, encryptionKey);

            System.out.print("cipher:  ");

            System.out.println(cipher);

            String decrypted = decrypt2(cipher, encryptionKey);

            System.out.println("decrypt: " + decrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void iniciarEncriptacion(String value) {
        try {

            System.out.println("==Java==");
            System.out.println("plain:   " + value);

            String cipher = encrypt2(value, encryptionKey);
            System.out.println("----------------------------------------------------------------");
            System.out.print("cipher:  ");

            System.out.println(cipher);
            System.out.println("----------------------------------------------------------------");
            String decrypted = decrypt2(cipher, encryptionKey);

            System.out.println("decrypt: " + decrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void iniciarDesencriptacion(String value) {
        try {

            System.out.print("cipher:  ");

            System.out.println(value);

            String decrypted = decrypt2(value, encryptionKey);

            System.out.println("decrypt: " + decrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String iniciarEncriptacion3(String value) {
        try {

            String cipher = encrypt2(value, encryptionKey);
            return cipher;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String iniciarDesencriptacion3(String value) {
        try {

            String decrypted = decrypt2(value, encryptionKey);
            return decrypted;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] encrypt(String plainText, String encryptionKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        // cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(encryptionKey.getBytes("UTF-8")));
        return cipher.doFinal(plainText.getBytes("UTF-8"));
    }

    public static String decrypt(byte[] cipherText, String encryptionKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        //cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(encryptionKey.getBytes("UTF-8")));
        return new String(cipher.doFinal(cipherText), "UTF-8");
    }

    public static String encrypt2(String plainText, String encryptionKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec key = new SecretKeySpec(getKeyBytes(encryptionKey), "AES");
        //cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(getKeyBytes(encryptionKey)));
        byte[] encValue = cipher.doFinal(plainText.getBytes("UTF-8"));
        String encryptedValue = Base64.encodeBase64String(encValue);
        return encryptedValue;
    }

    public static String decrypt2(String cipherText, String encryptionKey) throws Exception {
        byte[] decordedValue = Base64.decodeBase64(cipherText);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec key = new SecretKeySpec(getKeyBytes(encryptionKey), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(getKeyBytes(encryptionKey)));
        //cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
        return new String(cipher.doFinal(decordedValue), "UTF-8");
    }

    private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException {
        byte[] keyBytes = new byte[16];
        byte[] parameterKeyBytes = key.getBytes("UTF-8");
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0,
                Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }

    public static String hash(String password) {

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            //System.out.println("Hex format : " + sb.toString());
            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            //System.out.println("Hex format : " + hexString.toString());
            return hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static String hashHexMD5(String password) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            //System.out.println("Hex format : " + sb.toString());
            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            //System.out.println("Hex format : " + hexString.toString());
            return hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static String hashMD5(String password) {
        String hashedPassword;
        try {
            byte[] hash = MessageDigest.getInstance("MD5").digest(password.getBytes());
            hashedPassword = java.util.Base64.getEncoder().encodeToString(hash);
            //hashedPassword = Base64.getEncoder().encodeToString(clearTextPassword.getBytes());
            //System.out.println("Clear Text Password : " + clearTextPassword);
            //System.out.println("Encrypted Password : " + hashedPassword);
            
            return hashedPassword;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
