package util;

public class Constante {
	
	//// Bundle del sistema
	public static String bundleMensajeSistema = "/MensajeSistema";
	public static String bundleParametroSistema = "/ParametroSistema";

	//// 
	public static String ESTADO_REGISTRO_ACTIVO = "A";
	public static String ESTADO_REGISTRO_INACTIVO = "I";
	
	//// usuario
	public static String ESTADO_USUARIO_NUEVO = "S";
	public static String ESTADO_USUARIO_VIEJO = "N";
	
	//// estado mensaje
	public static String MSG_SUCCESS = "MSG_SUCCESS";
	public static String MSG_WARNING = "MSG_WARNING";
	public static String MSG_ERROR = "MSG_ERROR";
}
