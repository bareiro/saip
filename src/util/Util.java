package util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class Util {

	public static boolean isNotNullAndNotEmpty(String data) {
		return data != null && !data.trim().isEmpty();
	}
	
	public static String obtenerMensajeSistema(String key, Object... parametro) {
		return MessageFormat.format(ResourceBundle.getBundle(Constante.bundleMensajeSistema).getString(key), parametro);
	}
	
	public static String obtenerParametroSistema(String key, Object... parametro) {
		return MessageFormat.format(ResourceBundle.getBundle(Constante.bundleParametroSistema).getString(key), parametro);
	}
	
}
